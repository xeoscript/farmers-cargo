<?php

require_once './vendor/autoload.php';

function chopExtension($filename)
{
    return substr($filename, 0, strrpos($filename, '.'));
}

$loader = new Twig_Loader_Filesystem('./templates');
$twig   = new Twig_Environment($loader, array (
//    'cache' => './template-cache',
));

$name = $_SERVER['SCRIPT_FILENAME'];

$name   = basename($name, '.php');

echo $twig->render($name . '.twig', array ('name' => 'Fabien'));